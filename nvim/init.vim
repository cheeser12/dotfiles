set nocompatible               " vim, not vi :)

call plug#begin()
Plug 'sheerun/vim-polyglot'                                       " syntax packages
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " fuzzy finding
Plug 'junegunn/fzf.vim'
Plug 'tweekmonster/startuptime.vim'                               " useful for profiling slow plugins
Plug 'jnurmine/Zenburn'                                           " color scheme
Plug 'Shougo/deoplete.nvim'
Plug 'Shougo/neco-syntax'
call plug#end()

colorscheme zenburn

filetype plugin indent on      " load plugins according to detected filetype
syntax on                      " enable syntax highlighting

set number                     " line numbers!
set wildmenu                   " visual autocomplete for command menu
set showmatch                  " highlight matching [{()}]

set autoindent                 " indent according to previous line
set expandtab                  " spaces instead of tabs
set softtabstop=4              " Tab key indents by 4 spaces
set shiftwidth=4               " >> indents by 4 spaces
set shiftround                 " >> indents to next multiple of 'shiftwidth'

set backspace=indent,eol,start " make backspace work as you would expect
set hidden                     " switch between buffers without having to save first
set laststatus=2               " always show statusline
set display=lastline           " show as much as possible of the last line

set showmode                   " show current mode in command line
set showcmd                    " show already typed keys when more are expected

set incsearch                  " highlight while searching
set hlsearch                   " keep matches highlighted

set lazyredraw                 " only redraw when necessary

set splitbelow                 " open new windows below the current window
set splitright                 " open new windows right of the current window

set cursorline                 " find the current line quickly
set wrapscan                   " searches wrap around EOF
set report=0                   " always report changed lines

" show some whitespace characters (trail is particularly useful for finding
" trailing whitespace!)
set listchars=trail:·,precedes:«,extends:»,tab:▸\
set list

set foldenable        " enable folding
set foldlevelstart=10 " open most folds by default
set foldnestmax=10    " 10 nested fold max
set foldmethod=indent " fold based on indent level
" space open/closes folds
nnoremap <space> za

" move vertically by visual line
nnoremap j gj
nnoremap k gk

let mapleader=","

" turn off search highlight
nnoremap <Leader><space> :nohlsearch<CR>

" jk is escape
inoremap jk <esc>

" netrw settings
let g:netrw_banner = 0       " get rid of the banner
let g:netrw_liststyle = 3    " use tree view 

" fzf keybindings
map <Leader>b :Buffers<CR>
map <Leader>t :Files<CR>
map <Leader>r :Tags<CR>

" deoplete settings 
let g:deoplete#enable_at_startup = 1
